<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public static function getUser(Request $request)
    {
        $response = array();

        $user = auth('api')->user();

        $data = array();
        $data['user'] = $user;

        $response['data'] = $data;
        $response['message'] = 'User data fetched successful.';
        $response['status_code'] = Response::HTTP_OK;

        return response()->json($response)->setStatusCode($response['status_code']);
    }

    public static function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $response = array();

        try {
            $user = User::where('email', $email)
                ->first();

            if ($user != null) {
                if (Hash::check($password, $user->password)) {
                    // get password grant oauth client
                    $password_grant_client = DB::table('oauth_clients')->where('id', 2)->first();

                    $http = new Client([
                        'headers' => [
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/json'
                        ],
                        'timeout' => 10, // Response timeout
                        'connect_timeout' => 5, // Connection timeout
                    ]);

                    // send token request
                    $token_password_grant_route = (App::environment('local')) ? 'http://auth-web.test/oauth/token' : '/oauth/token';
                    $token_response = $http->post(url($token_password_grant_route), [
                        'form_params' => [
                            'grant_type' => 'password',
                            'client_id' => $password_grant_client->id,
                            'client_secret' => $password_grant_client->secret,
                            'username' => $email,
                            'password' => $password,
                            'scope' => [], // TODO :: Add scope based on user type permission
                        ],
                    ]);

                    $token = json_decode($token_response->getBody());
                    $token->expires_at = Carbon::now()->addSeconds($token->expires_in)->toDateTimeString();
                    Auth::login($user);

                    $data = array();
                    $data['user'] = $user;
                    $data['token'] = $token;

                    $response['data'] = $data;
                    $response['message'] = 'Login successful.';
                    $response['status_code'] = Response::HTTP_OK;
                } else {
                    $error = array();
                    $error['message'] = 'Invalid username/password.';

                    $response['error'] = $error;
                    $response['message'] = 'Login failed.';
                    $response['status_code'] = Response::HTTP_BAD_REQUEST;
                }
            } else {
                $error = array();
                $error['message'] = 'Provided email is not associated with an account.';

                $response['error'] = $error;
                $response['message'] = 'Login failed.';
                $response['status_code'] = Response::HTTP_BAD_REQUEST;
            }
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            // $error_code = $exception->errorInfo[1];

            $error = array();
            $error['message'] = 'Query exception occurred.';

            $response['error'] = $error;
            $response['message'] = 'Login failed.';
            $response['status_code'] = Response::HTTP_BAD_REQUEST;
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            $error = array();
            $error['message'] = 'Internal error occurred.';

            $response['error'] = $error;
            $response['message'] = 'Login failed.';
            $response['status_code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        Log::debug($response);
        return response()->json($response)->setStatusCode($response['status_code']);
    }

    public static function checkEmailAvailability($email)
    {
        $response = array();

        try {
            $emailAvailable = false;
            $user = User::where('email', $email)->first();
            if ($user == null) {
                $emailAvailable = true;
            }

            if ($emailAvailable) {
                $response['available'] = true;
                $response['message'] = 'Email is available for use.';
                $response['status_code'] = Response::HTTP_OK;
            } else {
                $error = array();
                $error['message'] = 'This email is already in use.';

                $response['error'] = $error;
                $response['available'] = false;
                $response['message'] = 'Please use a different email.';
                $response['status_code'] = Response::HTTP_CONFLICT;
            }
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            $error_code = $exception->errorInfo[1];

            if ($error_code == 1062) {
                $error = array();
                $error['message'] = 'This email is already in use.';

                $response['error'] = $error;
                $response['message'] = 'Please use a different email.';
                $response['status_code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
            } else {
                $error = array();
                $error['message'] = 'Query exception occurred.';

                $response['error'] = $error;
                $response['message'] = 'Failed to check email availability.';
                $response['status_code'] = Response::HTTP_BAD_REQUEST;
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            $error = array();
            $error['message'] = 'Internal error occurred.';

            $response['error'] = $error;
            $response['message'] = 'Failed to check email availability.';
            $response['status_code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        Log::debug($response);
        return response()->json($response)->setStatusCode($response['status_code']);
    }

    public static function signup(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        $response = array();

        try {
            DB::beginTransaction();

            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->save();

            DB::commit();

            $data = array();
            $data['user'] = $user;

            $response['data'] = $data;
            $response['message'] = 'Signup successful';
            $response['status_code'] = Response::HTTP_OK;
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            $error_code = $exception->errorInfo[1];

            if ($error_code == null) {
                $error = array();
                $error['message'] = 'Query exception occurred.';

                $response['error'] = $error;
                $response['message'] = 'Signup failed.';
                $response['status_code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
            } else if ($error_code == 1062) {
                $error = array();
                $error['message'] = 'Email is already in use.';

                $response['error'] = $error;
                $response['message'] = 'Signup failed.';

                $response['status_code'] = Response::HTTP_CONFLICT;
            } else {
                $error = array();
                $error['message'] = 'Query exception occurred.';

                $response['error'] = $error;
                $response['message'] = 'Signup failed.';
                $response['status_code'] = Response::HTTP_BAD_REQUEST;
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            $error = array();
            $error['message'] = 'Internal error occurred.';

            $response['error'] = $error;
            $response['message'] = 'Signup failed.';
            $response['status_code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        Log::debug($response);
        return response()->json($response)->setStatusCode($response['status_code']);
    }
}
