<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'user-name';
        $user->email = 'user@user.com';
        $user->password = \Illuminate\Support\Facades\Hash::make('user');
        $user->save();
    }
}
