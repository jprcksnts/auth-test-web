<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/login', 'AuthController@login');
Route::post('/user/signup', 'AuthController@signup');
Route::get('/user/email/{email}/availability', 'AuthController@checkEmailAvailability');
Route::get('/user', 'AuthController@getUser');
